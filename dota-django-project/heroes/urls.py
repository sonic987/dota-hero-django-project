from django.conf.urls import url
from . import views

app_name = 'hero'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^register/$', views.UserFormView.as_view(), name='register'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),

    # /dota/album/add
    url(r'hero/add/$', views.HeroCreate.as_view(), name='hero-add'),

    # /heroes/hero/2
    url(r'hero/(?P<pk>[0-9]+)/$', views.HeroUpdate.as_view(), name='hero-update'),

    # /heroes/hero/2/delete/
    url(r'hero/(?P<pk>[0-9]+)/delete/$', views.HeroDelete.as_view(), name='hero-delete'),
]
