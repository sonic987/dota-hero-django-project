from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse


class Hero(models.Model):
    CLASS_HERO = (
        ('Strength', 'Strength'),
        ('Intelligence', 'Intelligence'),
        ('Agility', 'Agility')
    )
    name = models.CharField(max_length=250)
    type = models.CharField(max_length=250)
    class_of_hero = models.CharField(max_length=15, choices=CLASS_HERO, default='Strength')
    hero_pic = models.FileField()

    def get_absolute_url(self):
        return reverse('hero:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name + '-' + self.type


class Guide(models.Model):
    hero = models.ForeignKey(Hero, on_delete=models.CASCADE)
    rating = models.CharField(max_length=2)
    guide_name = models.CharField(max_length=250)
    is_favorite = models.BooleanField(default=False)

    def __str__(self):
        return self.hero.name + '-' + self.guide_name
