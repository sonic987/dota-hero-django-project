from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.views import generic
from django.views.generic import View
from .models import Hero
from .forms import UserForm


class IndexView(generic.ListView):
    template_name = 'heroes/index.html'
    context_object_name = 'all_heroes'

    def get_queryset(self):
        return Hero.objects.all()


class DetailView(generic.DetailView):
    model = Hero
    template_name = 'heroes/detail.html'


class HeroCreate(CreateView):
    model = Hero
    fields = ['name', 'type', 'class_of_hero', 'hero_pic']


class HeroUpdate(UpdateView):
    model = Hero
    fields = ['name', 'type', 'class_of_hero', 'hero_pic']


class HeroDelete(DeleteView):
    model = Hero
    success_url = reverse_lazy('hero:index')


class UserFormView(View):
    form_class = UserForm
    template_name = 'heroes/registration_form.html'

    # blank form
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    # Process for data
    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():
            user = form.save(commit=False)

            # Cleaned (normalized) data
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()

            # returns user objects if credentials are correct
            user = authenticate(username=username, password=password)

            if user is not None:

                if user.is_active:
                    login(request, user)
                    return redirect('hero:index')

        return render(request, self.template_name, {'form': form})


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('hero:index')
