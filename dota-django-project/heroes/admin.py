from django.contrib import admin
from .models import Hero, Guide

admin.site.register(Hero)
admin.site.register(Guide)
